<?php

spl_autoload_register(static function (string $className) {
    require_once __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
});
