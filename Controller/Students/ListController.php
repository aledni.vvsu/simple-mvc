<?php

namespace Controller\Students;

use Http\Response;
use Model\Groups\GroupsProvider;
use Model\Students\StudentsProvider;
use View\Template;

class ListController
{
    private StudentsProvider $studentsProvider;
    private GroupsProvider $groupsProvider;

    public function __construct(
        StudentsProvider $studentsProvider,
        GroupsProvider $groupsProvider
    ) {
        $this->studentsProvider = $studentsProvider;
        $this->groupsProvider = $groupsProvider;
    }

    public function index(): Response
    {
        $groupId = $_GET['groupId'] ?? null;
        $groups = $this->groupsProvider->provideAll();

        if ($groupId) {
            $students = $this->studentsProvider->provideByGroup($groupId);
        }

        $view = new Template('students', [
            'students' => $students ?? [],
            'groups' => $groups,
        ]);

        return new Response($view());
    }
}
