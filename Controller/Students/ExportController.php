<?php

namespace Controller\Students;

use Exception;
use Http\Response;
use Model\Students\StudentsExporter;
use View\Template;

class ExportController
{
    private StudentsExporter $studentsExporter;

    public function __construct(StudentsExporter $studentsExporter)
    {
        $this->studentsExporter = $studentsExporter;
    }

    public function exportFromCsv(): Response
    {
        session_start();

        if ('POST' === $_SERVER['REQUEST_METHOD']) {
            $referrer = $_SERVER['HTTP_REFERER'];

            try {
                $file = $_FILES['file'];
                $this->studentsExporter->exportCsv($file);

                $_SESSION['_flush']['exportMessage'] = 'Данные успешно экспортированы';
            } catch (Exception $e) {
                $_SESSION['_flush']['exportMessage'] = 'Что-то пошло не так';
            }

            return new Response('', 302, ['Location: ' . $referrer]);
        }

        if (isset($_SESSION['_flush']['exportMessage'])) {
            $message = $_SESSION['_flush']['exportMessage'];
            unset($_SESSION['_flush']['exportMessage']);
        }

        $view = new Template('export', ['message' => $message ?? null]);

        return new Response($view());
    }
}
