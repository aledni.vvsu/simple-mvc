<?php

namespace Controller;

use Http\Response;
use View\Template;

class NotFoundController
{
    public function index(): Response
    {
        $view = new Template('404');

        return new Response($view(), 404);
    }
}
