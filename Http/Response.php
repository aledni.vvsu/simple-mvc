<?php

namespace Http;

class Response
{
    public string $content;
    public int $statusCode;
    public array $headers;

    public function __construct(string $content = '', int $statusCode = 200, $headers = [])
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function send(): void
    {
        http_response_code($this->statusCode);

        foreach ($this->headers as $header) {
            header($header);
        }

        echo $this->content;
    }
}
