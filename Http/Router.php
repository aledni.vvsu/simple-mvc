<?php

namespace Http;

class Router
{
    private array $routesConfig;

    public function __construct(array $routesConfig)
    {
        $this->routesConfig = $routesConfig;
    }

    public function match(string $uri): ?Route
    {
        $uriParts = explode('?', $uri);
        $clearUri = $uriParts[0];

        foreach ($this->routesConfig as $item) {
            $pattern = $item['pattern'];
            $isMatch = preg_match($pattern, $clearUri, $matches);

            if ($isMatch) {
                $route = new Route();
                $route->controller = $item['controller'];
                $route->action = $item['action'];
                $route->params = array_slice($matches, 1);

                return $route;
            }
        }

        return null;
    }
}
