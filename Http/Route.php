<?php

namespace Http;

class Route
{
    public string $controller;
    public string $action;
    public array $params = [];
}
