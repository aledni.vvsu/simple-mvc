<?php

namespace Database;

use PDO;

class Connection
{
    public PDO $pdo;

    public function __construct(array $config)
    {
        $this->pdo = new PDO(
            $config['dsn'],
            $config['user'],
            $config['password'],
        );
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function fetch(string $sql, array $params = []): ?array
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        return $stmt->fetch() ?: null;
    }

    public function fetchAll(string $sql, array $params = [], string $className = null): array
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        return $className ? $stmt->fetchAll(PDO::FETCH_CLASS, $className) : $stmt->fetchAll();
    }

    public function execute(string $sql, array $params = []): bool
    {
        return $this->pdo->prepare($sql)->execute($params);
    }
}
