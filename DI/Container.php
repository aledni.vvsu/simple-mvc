<?php

namespace DI;

use Exception;

class Container
{
    private array $cache = [];
    private array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function get(string $name)
    {
        if (array_key_exists($name, $this->cache)) {
            return $this->cache[$name];
        }

        if ( ! array_key_exists($name, $this->config)) {
            throw new Exception('[CONTAINER]: No such class');
        }

        if (class_exists($name)) {
            $dependencies = [];
            foreach ($this->config[$name] as $dependency) {
                $dependencies[] = $this->get($dependency);
            }

            $this->cache[$name] = $result = new $name(...$dependencies);
        } else {
            $this->cache[$name] = $result = & $this->config[$name];
        }

        return $result;
    }

    public function set(string $name, $object): self
    {
        $this->cache[$name] = $object;

        return $this;
    }
}
