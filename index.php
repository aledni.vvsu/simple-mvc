<?php

ini_set('display_errors', '1');

use Controller\NotFoundController;
use DI\Container;
use Http\Router;

require_once __DIR__ . '/autoload.php';

$routesConfig = include __DIR__ . '/routes-config.php';
$router = new Router($routesConfig);
$route = $router->match($_SERVER['REQUEST_URI']);

$containerConfig = include __DIR__ . '/container-config.php';
$container = new Container($containerConfig);

if ($route) {
    $controller = $container->get($route->controller);
    $action = $route->action;
    $params = $route->params;
}

$response = call_user_func_array(
    [
        $controller ?? $container->get(NotFoundController::class),
        $action ?? 'index'
    ],
    $params ?? []
);
$response->send();
