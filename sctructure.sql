create table `groups`
(
    id   int auto_increment        primary key,
    name varchar(20) not null,
    constraint groups_name_uindex unique (name)
) charset = utf8;

create table students
(
    id               int auto_increment   primary key,
    first_name       varchar(60)          not null,
    second_name      varchar(60)          not null,
    last_name        varchar(60)          not null,
    group_id         int                  not null,
    is_done_1st_task tinyint(1) default 0 null,
    is_done_2nd_task tinyint(1) default 0 null,
    is_done_3rd_task tinyint(1) default 0 null
) charset = utf8;

alter table students add constraint students_groups_fk foreign key (group_id) references `groups` (id);
