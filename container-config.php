<?php

use Controller\Students\ExportController;
use Controller\Students\ListController;
use Model\Groups\GroupsProvider;
use Model\Students\StudentsExporter;
use Controller\NotFoundController;
use Database\Connection;
use Model\Students\StudentsProvider;

return [
    NotFoundController::class => [],
    Connection::class => [
        'database-config',
    ],
    'database-config' => include __DIR__ . '/database-config.php',

    ExportController::class => [
        StudentsExporter::class,
    ],
    ListController::class => [
        StudentsProvider::class,
        GroupsProvider::class,
    ],

    StudentsExporter::class => [
        Connection::class,
    ],
    StudentsProvider::class => [
        Connection::class,
    ],
    GroupsProvider::class => [
        Connection::class,
    ]
];
