<?php
/**
 * @var string|null $message
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Экспорт данных</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
    <div class="container my-4">
        <a href="/">Список студентов</a>

        <h1>Экспорт данных</h1>

        <?php if ($message): ?>
            <div class="alert alert-primary d-inline-block my-4"><?=$message?></div>
        <?php endif ?>

        <form class="mt-4" method="post" enctype="multipart/form-data">
            <div class="custom-file">
                <label class="custom-file-label" for="file">Выберете файл со списком группы (csv)</label>
                <input type="file" class="custom-file-input" id="file" name="file" />
            </div>
            <button class="btn btn-primary mt-2">Загрузить</button>
        </form>
    </div>
</body>
</html>
