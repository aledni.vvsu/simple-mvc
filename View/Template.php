<?php

namespace View;

class Template
{
    private string $templateName;
    private array $params;

    public function __construct(string $templateName, array $params = [])
    {
        $this->templateName = $templateName;
        $this->params = $params;
    }

    public function __invoke(): string
    {
        ob_start();
        extract($this->params, EXTR_OVERWRITE);
        require __DIR__ . '/' . $this->templateName . '.php';
        $content = ob_get_contents();
        ob_clean();

        return $content;
    }
}
