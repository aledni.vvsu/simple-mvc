<?php
    /**
     * @var Model\Students\Student[] $students
     * @var Model\Groups\Group[] $groups
     */

    $estimateLabels = [
        2 => 'неудовлетворительно',
        3 => 'удовлетворительно',
        4 => 'хорошо',
        5 => 'отлично',
    ];
    $estimateClasses = [
        2 => 'badge badge-danger',
        3 => 'badge badge-warning',
        4 => 'badge badge-info',
        5 => 'badge badge-success',
    ];
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Список студентов</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
    <div class="container my-4">
        <a href="/export">Экспорт данных</a>

        <div class="mb-4 d-flex justify-content-between align-items-center">
            <h1 class="mr-4 mb-0">Список студентов <?=empty($students) ? '' : ('группы ' . $students[0]->groupName)?></h1>

            <form>
                <select class="form-control flex-shrink-0" name="groupId" onchange="this.closest('form').submit()">
                    <option value="" disabled selected>Выберете группу</option>
                    <?php foreach ($groups as $group): ?>
                        <option value="<?=$group->id?>"><?=$group->name?></option>
                    <?php endforeach; ?>
                </select>
            </form>
        </div>

        <?php if (empty($students)): ?>
            <div class="mt-4 alert alert-warning d-inline-block">Выберете группу</div>
        <?php else: ?>
            <table class="table table-striped mt-4">
                <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Первое задание</th>
                    <th>Второе задание</th>
                    <th>Третье задание</th>
                    <th>Оценка</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($students as $student): ?>
                    <?php $estimate = $student->getEstimate(); ?>
                    <tr>
                        <td><?=$student->lastName . ' ' . $student->firstName . ' ' . $student->secondName?></td>
                        <td><?php if ($student->isDone1stTask): ?><i class="bi bi-check-square"></i><?php endif; ?></td>
                        <td><?php if ($student->isDone2ndTask): ?><i class="bi bi-check-square"></i><?php endif; ?></td>
                        <td><?php if ($student->isDone3rdTask): ?><i class="bi bi-check-square"></i><?php endif; ?></td>
                        <td><div class="<?=$estimateClasses[$estimate]?>"><?=$estimateLabels[$estimate]?></div></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif ?>
    </div>
</body>
</html>
