<?php

namespace Model\Students;

use Database\Connection;
use Exception;

class StudentsExporter
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function exportCsv(array $file): void
    {
        if ('text/csv' !== $file['type']) {
            throw new Exception('File must be csv');
        }

        $groupName = str_replace('.csv', '', $file['name']);
        $filepath = $file['tmp_name'];

        $this->connection->execute(
            'DELETE FROM students WHERE group_id = (SELECT id FROM `groups` WHERE name = :name)',
            ['name' => $groupName],
        );
        $this->connection->execute('DELETE FROM `groups` WHERE name = :name', ['name' => $groupName]);
        $this->connection->execute('INSERT INTO `groups` (name) VALUES (:name)', ['name' => $groupName]);
        $groupId = $this->connection->pdo->lastInsertId();

        $f = new \SplFileObject($filepath, 'r');

        while ($row = $f->fgetcsv()) {
            $name = $row[0];
            [$lastName, $firstName, $secondName] = explode(' ', $name);
            $isDone1stTask = (int) ($row[1] ?? false);
            $isDone2ndTask = (int) ($row[2] ?? false);
            $isDone3rdTask = (int) ($row[3] ?? false);

            $this->connection->execute('
                INSERT INTO students (
                    first_name,
                    second_name,
                    last_name,
                    group_id,
                    is_done_1st_task,
                    is_done_2nd_task,
                    is_done_3rd_task
                ) VALUES (
                    :firstName,
                    :secondName,
                    :lastName,
                    :groupId,
                    :isDone1stTask,
                    :isDone2ndTask,
                    :isDone3rdTask
                )
            ', [
                'firstName' => $firstName,
                'secondName' => $secondName,
                'lastName' => $lastName,
                'groupId' => $groupId,
                'isDone1stTask' => $isDone1stTask,
                'isDone2ndTask' => $isDone2ndTask,
                'isDone3rdTask' => $isDone3rdTask,
            ]);
        }

        $f = null;
    }
}
