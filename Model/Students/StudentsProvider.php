<?php

namespace Model\Students;

use Database\Connection;

class StudentsProvider
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function provideByGroup(int $groupId): array
    {
        return $this->connection->fetchAll('
            SELECT 
                s.id id,
                s.first_name firstName,
                s.second_name secondName,
                s.last_name lastName,
                s.is_done_1st_task isDone1stTask,
                s.is_done_2nd_task isDone2ndTask,
                s.is_done_3rd_task isDone3rdTask,
                g.name groupName
            FROM students s
            JOIN `groups` g ON g.id = s.group_id
            WHERE g.id = :group_id
        ', [
            'group_id' => $groupId,
        ], Student::class);
    }
}