<?php

namespace Model\Students;

class Student
{
    public string $firstName;
    public string $secondName;
    public string $lastName;
    public string $groupName;
    public bool $isDone1stTask;
    public bool $isDone2ndTask;
    public bool $isDone3rdTask;

    public function getEstimate(): int
    {
        if ($this->isDone1stTask && $this->isDone2ndTask && $this->isDone3rdTask) {
            return 5;
        }

        if ($this->isDone1stTask && $this->isDone2ndTask) {
            return 4;
        }

        if ($this->isDone1stTask) {
            return 3;
        }

        return 2;
    }
}
