<?php

namespace Model\Groups;

class Group
{
    public int $id;
    public string $name;
    public int $studentsCount;
}
