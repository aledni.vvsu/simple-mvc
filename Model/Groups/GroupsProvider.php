<?php

namespace Model\Groups;

use Database\Connection;

class GroupsProvider
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function provideAll(): array
    {
        return $this->connection->fetchAll('
            SELECT g.id id, g.name name, count(s.id) studentsCount
            FROM `groups` g
            LEFT JOIN students s ON g.id = s.group_id
            GROUP BY g.id
        ', [], Group::class);
    }
}
