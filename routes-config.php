<?php

use Controller\Students\ExportController;
use Controller\Students\ListController;

return [
    [
        'pattern' => '/^\/$/',
        'controller' => ListController::class,
        'action' => 'index',
    ],
    [
        'pattern' => '/^\/export$/',
        'controller' => ExportController::class,
        'action' => 'exportFromCsv',
    ],
];
