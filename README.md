Архитектурная концепция MVC.
-

Приложение, следующее архитектуре MVC состоит из трех частей:
- Контроллеры
- Модели
- Представление. 

Каждая из этих частей имеет единственную, собственную ответственность.

Любое приложение имеет внутри маршрутизацию. Каждый его запуск начинается с определения правил, как обработать текущий маршрут.
Это ответственность роутера (машрутизатора).

Правила обработки маршрутов хранятся в конфигурации, например, в отдельном php файле в виде массива.
Правила представляют собой регулярное выражение, используемое как шаблон, для сопоставления с текущим маршрутом, и соответствующий обработчик маршрута.
Обработчик маршрута, чаще всего является методом одного из контроллеров.

Получив правила обработки маршурута, приложение инициализирует нужный контроллер и вызывает требуемый метод.
Ответственность контроллера заключается в формировании ответа для пользователя.
В контроллере можно извлекать параметры запроса, необходимые для формирования ответа, а так же формировать сам ответ.
Выполнение любой бизнес-логики делегируется модели, а отображение данных (формирование HTML, JSON и т.д.) делегируется представлению.

В модели выполняется какая-либо логика, формируются данные, необходимые для отображения пользователю.
В моделе можно выполнять запросы к базе данных, сторонним сервисам, проводить манипуляции с данными.
Ответственность модели заключается в формировании необходимых данных.

Ответственность представления заключается в отображении данных, например в формировании JSON структуры или HTML страницы, на основе каких-либо данных.
В представлении может присутствовать только логика, необходимая для отображения.

-----------------------------------------------

Ещё проще. Пользователь отправил запрос, приложение запустилось. Маршрутизатор определил нужный контроллер, и контроллер получил необходимые детали запроса, делегировав дальнейшее выполнение модели. Котроллер получил данные от модели и передал их представлению. Результат представления стал содержимым ответа пользователю.